# Capítulo 1

## Começando

A ideia inicial de um livro de programação é te levar a aprender a programar em uma linguagem especifica, geralmente ignorando todas as ferramentas utilizadas no momento na industria, minha intenção é diferente.: Quero começar o livro já com todas as ferramentas em uso, pra que os projetos nasçam corretamente, a organização do código seja fácil de seguir, e o resultado seja mais que correto: seja fácil de provar que é o correto.

Por isso, o código a seguir se chamará

## O Hello World mais difícil do mundo

Como todo livro de programação, começaremos a fazer um programa que escreve "Oi Mundo" na tela. Mas um "oi mundo" exige mais do que um código que escreva na tela do computador as letras pedidas: Existe as escolhas de projetos, escolhas de como começar a organizar o código, escolhas de como isso será visto em uma empresa ou grupo, então nosso primeiro projeto começa do inicio: Utilizando a linha de comando para instalar algumas coisas necessárias.

Adendo: Todo o código exibido aqui irá funcionar independente do sistema operacional, mas o command line irá ser focado em `command line` (as vezes simplesmente identificada como *cli*) Unix porque é o padrão utilizado no mundo para serviços web, caso você utilize windows, a maioria dos programas instalados também irá instalar o Mingw, que deve ser utilizado. Caso você use mac ou Linux, tanto faz.

O primeiro programa que precisamos instalar (porque, sim, necessariamente iremos precisar de mais de um programa) se chama `git` e é um sistema de versionamento para que não precisemos perder tempo no futuro buscando informação sobre o código em e-mails, aplicativos de compartilhamentos como Dropbox e outros, Google Docs ou pendrives espalhados em bolsos de calças, muito menos arquivos zip com nomes que ninguém mais entende depois de uma semana. Mas além do git iremos utilizar vários outros programas para escrever um simples hello world, e não existe outro exemplo mais correto de como fazer um hello world do que esse porque esse irá exibir todas as etapas que um software real precisa para ser considerado correto, de testes a integração continua. Uma pena que será também bem complexo.

Também evitaremos utilizar programas Desktop, com janelas, pois isso cria dependência do programador ao programa especifico, e não quero que ninguém dependa de algo para conseguir desenvolver, a grande maioria das coisas será feita em linha de comando.

Eu tentarei descrever tudo que irá aparecer na tela, mas escritor de primeira viagem, posso esquecer de algo que será acertado em versões futuras.

## Começando a organização inicial: Instalando o necessário e configurando o ambiente.

Nessa parte iremos instalar os programas necessários para ter um hello world funcional, e efetivamente começar, mas ainda sem programar.
Iniciaremos um repositório git na pasta de projetos e iremos criar um projeto de `C++` utilizando o `CMake` (por ser o padrão da industria), além de dar uma palhinha no interpretador de comandos de Unix

Instale o `git`, inicialmente esse é o passo mais importante pro projeto: é nele que iremos guardar as informações e arquivos.

Se você utiliza:
* Mac
    - Instale o `XCode Command Line Tools`
* Windows
    - vá em `www.git-scm.com` e baixe o instalador para windows
* Linux / BSD
    - Utilize o gerenciador de pacotes da sua distribuição para instalar o git
    - Linux não tem um padrão de instalação então não consigo deixar claro aqui qual seria o comando correto para instalar
        - Caso a distribuição seja Debian: apt
        - Caso a distribuição seja Fedora: dnf
        - Caso a distribuição seja Arch: pacman
        - E diversas outras - Procure a da sua distribuição e instale

Com o git instalado, precisamos abrir uma área para nosso projeto, para isso abra o programa de interpretador de comandos, no Linux você tem as opções de `konsole`, `gnome-terminal`, `xterm`, `urxvt` e muitos outros, no windows o programa vem junto com a instalação do `git`, e no mac o programa se chama `terminal`, mas você pode também instalar um substituto melhor chamado `iterm2`


Não adianta eu tentar colocar uma foto do meu terminal aqui porque cada desenvolvedor altera o terminal pra como lhe atende melhor, e as minhas configurações não serão as mesmas que as suas, para não confundir, usaremos a seguinte notação:

* Linhas iniciando por `$`: Usuário Não administrador
* Linhas iniciando por `#`: Usuário Administrador

Comandos iniciais para os não iniciados em linha de comando: (eu não entrarei muito a fundo na linha de comando, se você tem interesse de aprender de forma real o livro do Julio Neves - Papai do Shell - é uma indicação melhor)

* `pwd` - te dá o endereço da pasta que você está nesse momento
* `cd` - muda de pasta
* `mkdir` - cria uma pasta
* `touch` - cria um arquivo
* `~` - Significa "Pasta 'inicial' do usuário" 

Com isso, crie uma pasta onde seus projetos irão ficar, deixar tudo espalhado em sua pasta home não é uma solução boa. Em meu computador, eu crio uma pasta de projetos, e dentro da pasta de projetos a pasta do projeto que estamos começando:

```
$ mkdir Projetos

$ mkdir Projetos/OlaMundo  

$ cd Projetos/OlaMundo 

$ pwd
/home/tcanabrava/Projetos/OlaMundo

```

Com a pasta criada, e indo para dentro da pasta, iniciaremos a mesma como um repositório git:

```
~/Projetos/OlaMundo ⌚ 16:48:27
$ git init .
Initialized empty Git repository in /home/tcanabrava/Projetos/OlaMundo/.git/
```

Isso inicializa a pasta como um repositório git, você pode verificar a existência de uma nova pasta chamada `.git` dentro de nossa pasta de projeto utilizando seu navegador de arquivos preferido, ou o `ls`

```
$ pwd
/home/tcanabrava/Projetos/OlaMundo

$ ls -al
total 12
drwxr-xr-x 3 tcanabrava tcanabrava 4096 Nov  9 16:53 .
drwxr-xr-x 3 tcanabrava tcanabrava 4096 Nov  9 16:48 ..
drwxr-xr-x 7 tcanabrava tcanabrava 4096 Nov  9 16:51 .git
```

Sobre o comando `ls`:
* Serve para listar os arquivos dentro de pastas
* o -a serve para exibir arquivos ocultos, a pasta .git é oculta por padrão
* o -s serve para exibir os arquivo em uma lista vertical
* Você pode juntar vários comandos em um `-` só
    * ao invés de escrever `ls -a -l` pode fazer `ls -al`

Agora com o repositório do git criado, precisamos colocar algo dentro, e como devem ter notado, *ainda* não é o código do Ola Mundo. Tem motivo de eu chamar isso de `O olá mundo mais complicado do mundo`.

Mesmo sendo pequeno, Mesmo sendo um código exemplo, algumas padronizações são importantes em qualquer código porque assim outros programadores, sejam colegas, chefes ou espiões industriais, saibam por onde começar a olhar por seu projeto. Isso evolui sempre, projetos da década de 90 usavam conceitos que hoje em dia são vistos como hiper complicados.

Mas sabemos que uma organização de pastas é importante, e um texto pra nos dizer sobre o que o projeto se refere, também.

Antes de tudo, tenha a certeza de que está na pasta correta para o projeto, e tenha a certeza de que iniciou o repositório git na pasta:

```
$ pwd
/home/tcanabrava/Projetos/OlaMundo

$ git status
On branch master

No commits yet

nothing to commit (create/copy files and use "git add" to track)
```

E crie o primeiro arquivo do projeto, `README.md` O nome tem que necessariamente ser `README.md` pois todos os sistemas de gerenciamento de código online entendem esse formato, e virou um padrão não escrito. Usaremos o `touch` pra isso.

```
$ touch README.md

$ ls -la
total 12
drwxr-xr-x 3 tcanabrava tcanabrava 4096 Nov  9 17:11 .
drwxr-xr-x 3 tcanabrava tcanabrava 4096 Nov  9 16:48 ..
drwxr-xr-x 7 tcanabrava tcanabrava 4096 Nov  9 17:01 .git
-rw-r--r-- 1 tcanabrava tcanabrava    0 Nov  9 17:11 README.md
```

Agora, utilize o seu editor de texto preferido pra escrever algo no arquivo criado. O editor de texto **tanto faz**, mas não é um tanto faz real. você não pode utilizar Word, Office, LibreOffice, Wordpad, Notepad pra editar esse arquivo, porque esses programas colocam informações adicionais que **não são texto**, e não devem fazer parte do projeto. Programas estilo Office fazem texto para documentos impressos, com informações sobre fonte, tamanho de letra, se é ou não em negrito, cabeçalhos... Isso tudo é irrelevante para o que precisamos, então é hora de você escolher seu **editor de texto pra programação**, pode ser um da lista abaixo, pode ser outro que você goste, não pode ser projeto de pacote office.

* Visual Studio Code: https://code.visualstudio.com
* Atom: https://www.atom.io
* Kate: https://www.kate-editor.org
* Emacs: https://www.gnu.org/emacs
* Vim: https://www.vim.org

Abra e edite o arquivo README.md com o seguinte conteúdo:

```
# Projeto: Oi Mundo Mais Complicado do Mundo

Esse projeto serve como esboço do que é necessário em um projeto
para começar a trabalhar com C++ levando em consideração o mundo
real.
```

O formato `.md` significa `Markdown` e é um formato de texto puro que tem alguma informação de conteúdo. o `#` Cria uma linha grande, que se assemelha a um titulo, e parágrafos são espaçados em uma linha sem nada. Esse livro está sendo escrito em markdown, e o resultado final é isso que está lendo. Dá pra fazer muita coisa com isso.


<!---
Tomaz, o `ok` poderia ser trocado por um tudo bem não? Sugestão apenas.
--->

Mas, ok, arquivo criado e salvo, utilize o git para verificar o que mudou, e adicionar isso ao repositório:

```
$ git status
On branch master

No commits yet

Untracked files:
  (use "git add <file>..." to include in what will be committed)
        README.md

nothing added to commit but untracked files present (use "git add" to track)
```

Veja a linha contendo `Untracked files:`, essa é a lista de arquivos que o git ainda não sabe da existência. Precisamos adicionar o arquivo no índice de arquivos gerenciados pelo git.:

```
$ git add README.md 

$ git status
On branch master

No commits yet

Changes to be committed:
  (use "git rm --cached <file>..." to unstage)
        new file:   README.md
```

Agora a linha `Untracked files` sumiu, e uma novo bloco entrou, indicando que um novo arquivo foi colocado nas mudanças a serem adicionadas no repositório. Mas esse comando que demos não adicionou nada no repositório, e sim adicionou ao `Staging Area`, que é a área onde os arquivos ficam preparados para serem adicionados em uma revisão.

E adicionamos nossa `Staging Area` na revisão com git commit:

```
~/Projetos/OlaMundo on  master! ⌚ 18:34:20
$ git commit -m "Adicionado arquivo Readme."   
[master (root-commit) 478f2b6] Adicionado arquivo Readme.
 1 file changed, 6 insertions(+)
 create mode 100644 README.md
```

O parâmetro `-m` representa `Mensagem` e serve para o programador conseguir acompanhar o que acontece dentro do repositório. Agora, ao utilizar o comando `git show` temos a informação total do ultimo commit, com a mensagem, código e um numero identificador:

```
commit 478f2b65b962a81173b4a3fbe56985914239fcc0 (HEAD -> master)
Author: Tomaz Canabrava <tcanabrava@kde.org>
Date:   Mon Nov 9 18:34:42 2020 +0000

    Adicionado arquivo Readme.

diff --git a/README.md b/README.md
new file mode 100644
index 0000000..2d65b9e
--- /dev/null
+++ b/README.md
@@ -0,0 +1,6 @@
+# Projeto: Oi Mundo Mais Complicado do Mundo
+
+Esse projeto serve como esboço do que é necessário em um projeto
+para começar a trabalhar com C++ levando em consideração o mundo
+real.
+
(END)
```

Aperte `q` para sair dessa tela dentro do git.

## Configurando o Projeto

Para configurar o projeto, utilizaremos o CMake, que é o gerenciador padrão da industria para projetos em C++, e portanto é o com maior documentação e com mais projetos utilizando. CMake serve para traduzir as informações do código fonte, a sua organização em disco, e quais projetos você está fazendo dentro de seu código para as ferramentas da linguagem - como `compiladores`, `debuggers`, `IDEs`.

Lembrando do que queremos fazer: Um programa simples que ao ser executado escreva `Oi Mundo` na tela, vamos preparar nosso arquivo `CMakeLists.txt` pra isso, junto com nossas pastas. Criarei uma pasta chamada `src` de `source` para colocar nosso código dentro, assim o código fonte não ficará espalhado pela pasta principal, já que a pasta principal tem conteúdo que não é necessariamente código fonte, como o README.md - e mais coisas que serão adicionadas no futuro

```
$ pwd
/home/tcanabrava/Projetos/OlaMundo

$ touch CMakeLists.txt

$ mkdir src

$ touch src/main.cpp

$ touch src/CMakeLists.txt
```

Isso criará o esquema de pastas e arquivos necessários para compilarmos o nosso primeiro programa, mas ainda temos que preencher o CMakeLists.txt de forma que o CMake consiga gerar o projeto. Para verificação, nosso sistema de pastas e arquivos no momento é esse:

```
$ tree                    
.
├── CMakeLists.txt
├── README.md
└── src
    ├── CMakeLists.txt
    └── main.cpp

1 directory, 4 files
```

O comando `tree` exibe as pastas em forma de arvore, muito útil para fazer uma verificação rápida do estado dos seus arquivos.
Agora temos dois arquivos CMakeLists.txt, um em cada pasta. O que está na pasta raiz do projeto irá definir as coisas base para todos os projetos que farão parte de nosso código, e irá também adicionar a pasta `src` no meio.


```
CMakeLists.txt
--------------

project(OlaMundo)

cmake_minimum_required(VERSION 3.19)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

add_subdirectory(src)
```

A primeira linha `project(OlaMundo)` define o nome desse projeto para o CMake, mas isso não é o nome do programa que iremos criar, já que podemos criar várias coisas ao mesmo tempo, e mais de um programa ao mesmo tempo. Isso é só um nome geral do que você quer com seu software.

A segunda linha define a versão minima de CMake que seu código irá rodar.


As seguintes linhas:
```
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
```

Definem qual a versão minima de C++ aceita para o projeto. A linguagem C++ foi criada a mais de 30 anos, e evoluções aconteceram em 1998, 2003, 2011, 2014, 2017 e 2020. Escolher uma versão muito antiga te deixa de fora de muitas coisas novas e interessantes da linguagem, e escolher uma muito nova pode esbarrar em um bug de compilador, ou em uma feature não implementada.
Antes de decidir qual versão do c++ você irá utilizar para seu projeto, verifique a tabela de compiladores em https://en.cppreference.com/w/cpp/compiler_support para ver se o que você vai utilizar tem um suporte bom para a versão do c++ que quer utilizar.

No momento, a ultima versão do C++ com bom suporte em todos os compiladores é a 2017, por isso estarei utilizando a mesma pelo livro.

A ultima linha adiciona a pasta de nome `src` ao projeto, essa pasta irá conter todos os subprojetos (executaveis, bibliotecas internas) que iremos fazer durante o livro. Dentro dela se encontra outro arquivo CMakeLists.txt, que descreve o projeto a partir dessa pasta, e um arquivo .cpp que conterá o código para nosso primeiro executavel.

O `CMakeLists.txt` da pasta `src` inicialmente é bem pequeno, queremos que ele compile o arquivo main.cpp em um executavel. o comando pra isso via cmake é `add_executable`, e recebe o nome do programa que será gerado, e a lista de arquivos que farão parte do programa:

```
src/CMakeLists.txt
------------------

add_executable(HelloWorld main.cpp)
```

E Nosso programa é o mais simples possivel, um "hello world", o classico programa que escreve um Olá Mundo na tela, e sai

```
src/main.cpp
------------
#include <iostream>  // std::cout
#include <cstdlib>   // EXIT_SUCCESS 

int main() {
    std::cout << "Hello World" << std::endl;
    return EXIT_SUCCESS;
}
```

Simples, conciso, funcional. A unica diferenca real desse código para milhares de outros códigos em c++ de outros livros é a adição do `cstdlib` para o `EXIT_SUCCESS`, pois se é ensinado que no fim do main você retorna zero, mas isso não diz nada para quem está aprendendo, e é muito mais legivel se retornar sucesso ou erro. o `cstdlib` possui EXIT_SUCCESS e EXIT_FAILURE para indicar quando algo deu errado ou não, e mesmo que não adicione em nada no código (porque EXIT_SUCCESS é definido como Zero), adiciona ao programador e ao leitor informação sobre a razão de existência da função.

Agora precisamos Configurar como o CMake irá configurar nossos projetos, Digitar comandos manualmente na linha de comando nem sempre é uma boa idéia pois é muito fácil acontecerem `typos` pequenos que nos fazem quebrar a cabeça por horas. Sempre é melhor criar uma espécie de configuração base que nos ajuda a organizar todo o projeto. O `CMake`, a partir da versão 3.19, possui um comando `--preset` que utiliza uma configuração pre-determinada para configurar os projetos. 

Crie um arquivo chamado CMakePreset.json, na pasta root do projeto:

```
CMakePreset.json
----------------
{
    "version": 1,
    "cmakeMinimumRequired": {
      "major": 3,
      "minor": 19,
      "patch": 0
    },
    "configurePresets": [
        {
            "name": "debug",
            "displayName": "Compilacao Debug",
            "description": "Compila em modo debug usando make",
            "generator": "Unix Makefiles",
            "binaryDir": "${sourceDir}/../build/helloworld/debug",
            "cacheVariables": {
                "CMAKE_BUILD_TYPE": "Debug"
            }
        },
        {
            "name": "release",
            "displayName": "Compilacao Release",
            "description": "Compila em modo release utilizando o Ninja",
            "generator": "Ninja",
            "binaryDir": "${sourceDir}/../build/helloworld/release",
            "cacheVariables": {
                "CMAKE_BUILD_TYPE": "Release"
            }
        }
    ]
}
```

Json é um formato de arquivo de texto para descrição de dados, nesse configuramos dois tipos de build, um utilizando Unix Makefiles. O tipo de generator depende de seu sistema operacional e dos softwares que você tem instalado. Caso você utilize linux, os listados acima estão corretos, caso utilize outro sistema, não. Pegue a lista de geradores para sua plataforma utilizando o comando `cmake --help` e olhe a lista de geradores possiveis para sua plataforma.

Com essa configuração criamos dois geradores:
- Debug, que utiliza o Unix Makefiles. A escolha do Unix Makefiles para o gerador de Debug é por ele ser sequencial, onde cada arquivo espera sua vez para ser compilado, assim ao dar erros fica mais fácil de identificar onde deu.

- Release, que utiliza o Ninja. No modo release os erros de compilação não devem existir pois ja foram removidos no sequencial, e podemos utilizar todos os cores de nosso processador.

Com o arquivo feito, teste se os presets estão sendo corretamente carregados:
```
$ pwd
/home/tcanabrava/Projetos/OlaMundo

$ ls
CMakeLists.txt  CMakePresets.json  README.md  src

$ cmake . --list-presets  
Available presets:

  "debug"   - Compilacao Debug
  "release" - Ninja
```

E agora, configure o projeto utilizando o preset escolhido.

```
$ cmake . --preset=debug 
Preset CMake variables:

  CMAKE_BUILD_TYPE="Debug"

-- The C compiler identification is GNU 10.2.0
-- The CXX compiler identification is GNU 10.2.0
-- Detecting C compiler ABI info
-- Detecting C compiler ABI info - done
-- Check for working C compiler: /usr/bin/cc - skipped
-- Detecting C compile features
-- Detecting C compile features - done
-- Detecting CXX compiler ABI info
-- Detecting CXX compiler ABI info - done
-- Check for working CXX compiler: /usr/bin/c++ - skipped
-- Detecting CXX compile features
-- Detecting CXX compile features - done
-- Configuring done
-- Generating done
-- Build files have been written to: /home/tcanabrava/Projetos/build/helloworld/debug
```

Veja que a ultima linha indica onde os arquivos de build do projeto foram parar. Ela é a linha definida em `binaryDir` no seu preset.
Agora vá para onde seu projeto foi configurado, e utilize `make` caso tenha utilizado o preset com Unix Makefiles, ou `ninja`, caso tenha utilizado o preset de release.

```
$ cd /home/tcanabrava/Projetos/build/helloworld/debug

$ make
Scanning dependencies of target HelloWorld
[ 50%] Building CXX object src/CMakeFiles/HelloWorld.dir/main.cpp.o
[100%] Linking CXX executable HelloWorld
[100%] Built target HelloWorld

```

E temos o nosso primeiro projeto compilado, mas ainda falta muito para terminar o capitulo, afinal esse é o Olá Mundo mais complicado do mundo.

Um resumo do que temos até agora
```
$ tree
.
├── CMakeLists.txt
├── CMakePresets.json
├── README.md
└── src
    ├── CMakeLists.txt
    └── main.cpp

1 directory, 5 files
```
